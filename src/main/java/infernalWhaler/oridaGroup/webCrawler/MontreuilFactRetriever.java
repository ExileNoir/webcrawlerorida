package infernalWhaler.oridaGroup.webCrawler;

import infernalWhaler.oridaGroup.beans.Project;
import infernalWhaler.oridaGroup.client.GenericClient;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * MontreuilFactRetriever partOne
 *
 * @author sDeseure
 */

public class MontreuilFactRetriever extends GenericClient {

    public void onCreate() throws Exception {
        System.setProperty("webdriver.chrome.driver", "C:\\webdriver\\chromedriver.exe");
        createChromeDriver();

        TimeUnit.SECONDS.sleep(1);
        getDriver().get("https://budgetparticipatif.montreuil.fr/project/budget-participatif-2017/selection/resultats");
        TimeUnit.SECONDS.sleep(1);
    }

    public void loadAllAvailable() {
        final List<WebElement> listProjectElements = getDriver().findElements(By.xpath("//ul[@class='media-list proposal-preview-list opinion__list']/li"));
        final List<Project> listProjects = new ArrayList<>();

        for (final WebElement elemProject : listProjectElements) {
            parseProject(listProjects, elemProject);
        }
        // todo System.out.print just as test!
        System.out.print(listProjects);
    }

    private void parseProject(final List<Project> listProjects, final WebElement elemProject) {
        final String title = elemProject.findElement(By.xpath(".//h4[@class='Title__Container-sc-16bpy8r-0 card__title cBNFVM']/span")).getText().trim().replace("\n", " ");
        final String announcer = elemProject.findElement(By.xpath(".//div[@class='Body__Container-sc-101yb2c-0 daTwhp media-body']/a")).getText().trim();
        final String date = elemProject.findElement(By.xpath(".//div[@class='excerpt small']")).getText().trim();
        final String costPrice = elemProject.findElement(By.xpath(".//span[@class='Tag__TagContainer-j1d5ea-0 dpnsBo']/span")).getText().trim();
        final String place = elemProject.findElement(By.xpath("//div[@class='tags-list ellipsis TagsList__TagsListWrapper-sc-8sbv66-0 gCzwqA']/div[3]/span")).getText().trim();
        final String description = elemProject.findElement(By.xpath(".//p[@class='excerpt small']")).getText().trim();
        final String status = elemProject.findElement(By.xpath(".//div[@class='Status__Container-sc-1ej19hn-0 ellipsis card__status joDlGM' " +
                "or @class='Status__Container-sc-1ej19hn-0 ellipsis card__status huGXyJ' " +
                "or @class='Status__Container-sc-1ej19hn-0 ellipsis card__status kzRuRD' " +
                "or @class='Status__Container-sc-1ej19hn-0 ellipsis card__status iPRuNj']")).getText().trim();
        final String votes; // todo did not find element for parsing "votes"

        addProject(listProjects, title, announcer, date, costPrice, place, description, status);
    }

    private void addProject(final List<Project> listProjects, final String title, final String announcer, final String date, final String costPrice, final String place, final String description, final String status) {
        final Project project = new Project(title, announcer, date, costPrice, place, description, status, "");
        if (listProjects.contains(project)) {
            return;
        }
        listProjects.add(project);
    }

    // todo psvm inClass just as test
    public static void main(String[] args) throws Exception {
        final MontreuilFactRetriever m = new MontreuilFactRetriever();
        m.onCreate();
        m.loadAllAvailable();
    }
}
