package infernalWhaler.oridaGroup.beans;

public class Project {

    private String title;
    private String announcer;
    private String date;
    private String costPrice;
    private String place;
    private String description;
    private String status;
    private String votes;


    public Project(String title, String announcer, String date, String costPrice, String place, String description, String status, String votes) {
        this.title = title;
        this.announcer = announcer;
        this.date = date;
        this.costPrice = costPrice;
        this.place = place;
        this.description = description;
        this.status = status;
        this.votes = votes;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAnnouncer() {
        return announcer;
    }

    public void setAnnouncer(String announcer) {
        this.announcer = announcer;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(String costPrice) {
        this.costPrice = costPrice;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVotes() {
        return votes;
    }

    public void setVotes(String votes) {
        this.votes = votes;
    }

    @Override
    public String toString() {
        return getTitle() + " : " + getDate();
    }


}
