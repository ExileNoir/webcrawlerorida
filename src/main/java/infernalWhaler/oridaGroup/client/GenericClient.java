package infernalWhaler.oridaGroup.client;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public abstract class GenericClient {

    private WebDriver driver;

    protected void createChromeDriver() {
        driver = new ChromeDriver();
    }

    protected WebDriver getDriver() {
        return driver;
    }
}
