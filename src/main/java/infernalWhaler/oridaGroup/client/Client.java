package infernalWhaler.oridaGroup.client;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

public class Client {


    private String baseUrl = "";
    private WebClient client = new WebClient();

    public void setCssEnabled() {
        client.getOptions().setCssEnabled(false);
    }

    public void setJavaScriptEnabled() {
        client.getOptions().setJavaScriptEnabled(false);
    }

    public void setUseInsecureSSL(){
        client.getOptions().setUseInsecureSSL(true);
    }

    public HtmlPage getPage()throws Exception{
        return client.getPage(getBaseUrl());
    }



    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }
}
